package kirbyutils.bukkit.compat;

import kirbyutils.bukkit.compat.nms.CompatProtocolLib;
import kirbyutils.bukkit.compat.nms.CompatStub;
import org.bukkit.plugin.java.JavaPlugin;

public class NMSLoader {

    private static final String NMS_CLASS_FORMAT = "kirbyutils.compat.nms.Compat";

    private static final String[] nmsClasses = {
            "1_11_R1"
    };


    /**
     * Load an NMS compatibility layer
     *
     * @param plugin The Bukkit plugin loading the layer
     * @return A NMS layer, or null
     */
    public static NMS getNMS(JavaPlugin plugin) {
        return getNMS(plugin, true);
    }

    public static NMS getNMS(JavaPlugin plugin, boolean protocolLib) {
        plugin.getLogger().info("[NMS COMPAT] Attempting to load NMS compatibility...");
        // Attempt to load ProtocolLib first
        if (plugin.getServer().getPluginManager().getPlugin("ProtocolLib") != null && protocolLib) {
            plugin.getLogger().info("[NMS COMPAT] ProtocolLib detected, using ProtocolLib");
            CompatProtocolLib compat = new CompatProtocolLib();
            compat.enable(plugin);
            return compat;
        } else {
            for (String name : nmsClasses) {
                name = NMS_CLASS_FORMAT + name;
                try {
                    NMS nms = (NMS) Class.forName(name).newInstance();
                    plugin.getLogger().info("[NMS COMPAT] Using compatibility class " + nms.getClass().getCanonicalName() + " implemeneting NMS version " + nms.getNMSVersion());
                    nms.enable(plugin);
                    return nms;
                } catch (Exception e) {
                    // Ignore
                }
            }
        }
        plugin.getLogger().info("[NMS COMPAT] === WARNING ===");
        plugin.getLogger().info("[NMS COMPAT] NMS Class Not Found! Loading stub class");
        NMS stub = new CompatStub();
        stub.enable(plugin);
        return stub;
    }
}
