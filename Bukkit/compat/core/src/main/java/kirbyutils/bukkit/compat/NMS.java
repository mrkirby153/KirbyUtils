package kirbyutils.bukkit.compat;

import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public interface NMS {

    /**
     * Send an action bar to the player
     *
     * @param player    The player to send the action bar to
     * @param component The component to send to the player
     */
    void actionBar(Player player, BaseComponent component);

    /**
     * Called when the compat class is loaded
     *
     * @param plugin The plugin loading the class
     */
    void enable(JavaPlugin plugin);

    /**
     * Gets the NMS version this class is compatible for
     *
     * @return The NMS version compatible
     */
    String getNMSVersion();

    /**
     * Sends a title to a player with the default timings
     *
     * @param player   The player to send the title to
     * @param title    The title to send
     * @param subtitle The subtitle to send
     */
    void title(Player player, String title, String subtitle);

    /**
     * Sends a title the a player
     *
     * @param player       The player to send the title to
     * @param title        The title to send
     * @param subtitle     The subtitle to send
     * @param fadeInTicks  The fade in time (in ticks)
     * @param stayTicks    The stay time (in ticks)
     * @param fadeOutTicks The fade out time (in ticks)
     */
    void title(Player player, String title, String subtitle, int fadeInTicks, int stayTicks, int fadeOutTicks);
}
