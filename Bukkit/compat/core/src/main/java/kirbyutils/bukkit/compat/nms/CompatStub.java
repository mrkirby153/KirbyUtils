package kirbyutils.bukkit.compat.nms;

import kirbyutils.bukkit.compat.NMS;
import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class CompatStub implements NMS {

    private boolean warned = false;
    private JavaPlugin plugin = null;

    @Override
    public void actionBar(Player player, BaseComponent component) {
        warn();
    }

    @Override
    public void enable(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public String getNMSVersion() {
        return "STUB";
    }

    @Override
    public void title(Player player, String title, String subtitle) {
        warn();
    }

    @Override
    public void title(Player player, String title, String subtitle, int fadeInTicks, int stayTicks, int fadeOutTicks) {
        warn();
    }

    public void warn() {
        if(warned)
            return;
        plugin.getLogger().warning("============================================================================");
        plugin.getLogger().warning("Could not find a NMS compatibility class for this version. NMS WILL NOT WORK");
        plugin.getLogger().warning("============================================================================");
        warned = true;
    }
}
