package kirbyutils.bukkit.module;

import co.aikar.taskchain.BukkitTaskChainFactory;
import co.aikar.taskchain.TaskChain;
import co.aikar.taskchain.TaskChainFactory;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * An abstract framework for modules
 *
 * @param <T>
 */
public abstract class Module<T extends JavaPlugin> {

    private T plugin;
    private String name = "UNKNOWN";
    private boolean loaded = false;

    private TaskChainFactory taskChainFactory;

    public Module(T plugin) {
        this.plugin = plugin;
        taskChainFactory = BukkitTaskChainFactory.create(plugin);
    }

    /**
     * Gets the module's configuration section
     *
     * @return The module's configuration setting
     */
    public ConfigurationSection configSection() {
        return getConfig().getConfigurationSection(this.name);
    }

    /**
     * Gets the <b>parent</b> plugin's configuration file
     *
     * @return The configuration file
     */
    public FileConfiguration getConfig() {
        return plugin.getConfig();
    }

    /**
     * Gets the name of this module
     *
     * @return The name of this module
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the module
     *
     * @param name The name of the module to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Load the module
     */
    public void load() {
        if (this.loaded) {
            return;
        }

        // Fire an event before loading
        ModuleLoadedEvent event = new ModuleLoadedEvent(this);
        this.plugin.getServer().getPluginManager().callEvent(event);
        if (event.isCancelled())
            return; // Abort load if canceled

        // Load the module
        log("Loading");
        onEnable();
    }

    /**
     * Logs a message
     *
     * @param message The message to log
     */
    public void log(String message) {
        this.plugin.getLogger().info("[" + getName().toUpperCase() + "] " + message);
    }

    /**
     * Gets a new {@link TaskChain}
     *
     * @return The task chain
     */
    public <H> TaskChain<H> newChain() {
        return this.taskChainFactory.newChain();
    }

    /**
     * Gets a new Shared {@link TaskChain}
     *
     * @param name The name of the chain
     */
    public <H> TaskChain<H> newSharedChain(String name) {
        return taskChainFactory.newSharedChain(name);
    }

    /**
     * Called when the module is disabled
     */
    public abstract void onDisable();

    /**
     * Called when the module is enabled
     */
    public abstract void onEnable();

    /**
     * Registers a listener
     *
     * @param listener The listener to register
     */
    public void registerListener(Listener listener) {
        this.plugin.getServer().getPluginManager().registerEvents(listener, this.plugin);
    }

    /**
     * Schedules a task using the bukkit schedule to run later
     *
     * @param runnable The runnable to run
     * @param delay    The delay (in ticks)
     */
    public void runLater(Runnable runnable, int delay) {
        this.plugin.getServer().getScheduler().runTaskLater(this.plugin, runnable, delay);
    }

    /**
     * Schedules a task using the bukkit scheduler to run later async
     *
     * @param runnable The runnbale to run
     * @param delay    The delay (in ticks)
     */
    public void runLaterAsync(Runnable runnable, int delay) {
        this.plugin.getServer().getScheduler().runTaskLaterAsynchronously(this.plugin, runnable, delay);
    }

    /**
     * Schedules a repeating task using the bukkit scheduler
     *
     * @param runnable The task to run
     * @param delay    The initial delay
     * @param period   The repeat frequency
     */
    public void scheduleRepeating(Runnable runnable, int delay, int period) {
        this.plugin.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, runnable, delay, period);
    }

    /**
     * Unload the module
     */
    public void unload() {
        if (!this.loaded)
            return;

        ModuleUnloadedEvent event = new ModuleUnloadedEvent(this);
        this.plugin.getServer().getPluginManager().callEvent(event);
        if (event.isCancelled())
            return; // Abort unload if canceled

        log("Unloading");
        onDisable();
    }

}
