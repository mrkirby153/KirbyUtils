package kirbyutils.bukkit.module;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ModuleUnloadedEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();

    private Module<?> module;
    private boolean canceled = false;

    public ModuleUnloadedEvent(Module<?> module) {
        this.module = module;
    }

    @Override
    public HandlerList getHandlers() {
        return null;
    }

    /**
     * Gets the module being unloaded
     *
     * @return The module being unloaded
     */
    public Module<?> getModule() {
        return module;
    }

    @Override
    public boolean isCancelled() {
        return this.canceled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        this.canceled = cancel;
    }
}
