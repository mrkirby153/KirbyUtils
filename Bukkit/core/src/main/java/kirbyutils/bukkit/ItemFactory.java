package kirbyutils.bukkit;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.enchantments.EnchantmentWrapper;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * A basic factory for easily constructing items
 */
public class ItemFactory {

    public static final Enchantment ENCHANT_GLOWING = new GlowEnchantment();

    private final Material material;
    private short data;
    private String name;
    private int amount;
    private boolean unbreakable;
    private List<String> lore;
    private List<ItemFlag> flags;

    private HashMap<Enchantment, Integer> enchantments;

    public ItemFactory(Material material) {
        this.material = material;
        this.data = 0;
        this.amount = 1;
        this.unbreakable = false;
        this.lore = new ArrayList<>();
        this.flags = new ArrayList<>();
        this.enchantments = new HashMap<>();
    }

    /**
     * Sets the amount of items
     *
     * @param amount The amount of items
     * @return The factory
     */
    public ItemFactory amount(int amount) {
        this.amount = amount;
        return this;
    }

    /**
     * Constructs the item
     *
     * @return The item
     */
    public ItemStack construct() {
        ItemStack stack = new ItemStack(this.material, this.amount, this.data);
        ItemMeta meta = stack.getItemMeta();
        if (name != null)
            meta.setDisplayName(ChatColor.RESET + this.name);
        meta.setLore(this.lore);
        meta.setUnbreakable(this.unbreakable);
        meta.addItemFlags(this.flags.toArray(new ItemFlag[0]));
        this.enchantments.forEach((e, l) -> meta.addEnchant(e, l, true));
        stack.setItemMeta(meta);
        return stack;
    }

    /**
     * Adds an enchantment
     *
     * @param e     The enchantment to add
     * @param level The level of the enchantment
     * @return The factory
     */
    public ItemFactory enchantment(Enchantment e, int level) {
        this.enchantments.put(e, level);
        return this;
    }

    /**
     * Adds an item flag
     *
     * @param flags The item's flag
     * @return The factory
     */
    public ItemFactory flags(ItemFlag... flags) {
        this.flags.addAll(Arrays.asList(flags));
        return this;
    }

    /**
     * Makes the item glow
     *
     * @return The factory
     */
    public ItemFactory glowing() {
        this.enchantment(ENCHANT_GLOWING, 1);
        return this;
    }

    /**
     * Adds to the item's lore
     *
     * @param lore The lore of the item
     * @return The factory
     */
    public ItemFactory lore(String... lore) {
        this.lore.addAll(Arrays.asList(lore));
        return this;
    }

    /**
     * Sets the item's name
     *
     * @param name The name of the item
     * @return The factory
     */
    public ItemFactory name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Sets the item's data
     *
     * @param data The data
     * @return The factory
     */
    public ItemFactory setData(short data) {
        this.data = data;
        return this;
    }

    /**
     * Sets the item unbreakable
     *
     * @return The factory
     */
    public ItemFactory unbreakable() {
        this.unbreakable = true;
        return this;
    }

    public static class GlowEnchantment extends EnchantmentWrapper {

        public GlowEnchantment() {
            super(120);
            register();
        }

        @Override
        public boolean canEnchantItem(ItemStack item) {
            return true;
        }

        @Override
        public boolean conflictsWith(Enchantment other) {
            return false;
        }

        @Override
        public EnchantmentTarget getItemTarget() {
            return null;
        }

        @Override
        public int getMaxLevel() {
            return 1;
        }

        @Override
        public String getName() {
            return "NullEnchantment";
        }

        @Override
        public int getStartLevel() {
            return 1;
        }

        private void register() {
            try {
                // Register enchantment
                Field f = Enchantment.class.getDeclaredField("acceptingNew");
                f.setAccessible(true);
                f.setAccessible(true);
                boolean previous = f.getBoolean(null);
                f.set(null, true);
                if (Enchantment.getByName("NullEnchantment") == null)
                    Enchantment.registerEnchantment(this);

                f.setBoolean(null, previous);
            } catch (IllegalAccessException | NoSuchFieldException e) {
                e.printStackTrace();
            }
        }
    }
}
